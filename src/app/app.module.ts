import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import { ArrComponent } from './arr/arr.component';
import { DemoComponent } from './demo/demo.component';
import { HomeComponent } from './home/home.component';

import { DataService } from './data.service';
import { AboutComponent } from './about/about.component';
import { FooterComponent } from './footer/footer.component';


@NgModule({
  declarations: [
    AppComponent,
    ArrComponent,
    DemoComponent,
    HomeComponent,
    AboutComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
   
  ],
  
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
